from argparse import ArgumentParser, Namespace, RawTextHelpFormatter
from typing import Any, List

import argcomplete

from wapt.actions.install import install
from wapt.actions.list import list_action
from wapt.config.config import config_list_packages


def _package_completer(prefix: str, **_: Any) -> List[str]:
    packages = config_list_packages()
    return [package for package in packages if package.startswith(prefix)]


def _install(args: Namespace):
    install(args.packages)


def _list(args: Namespace):
    packages = args.packages if args.packages else config_list_packages()
    list_action(packages, args.all_versions)


def main():
    parser = ArgumentParser(description="")
    subparsers = parser.add_subparsers(dest="action", metavar="<action>")
    subparsers.required = True

    subparser = subparsers.add_parser("install", help="", formatter_class=RawTextHelpFormatter)
    subparser.set_defaults(action=_install)
    argument = subparser.add_argument("packages", nargs="+", help="List of packages to install.")
    argument.completer = _package_completer  # type: ignore

    subparser = subparsers.add_parser("list", help="", formatter_class=RawTextHelpFormatter)
    subparser.set_defaults(action=_list)
    subparser.add_argument("--all-versions", "-a", action="store_true", help="List of packages to install.")
    argument = subparser.add_argument("packages", nargs="*", help="List of packages to install.")
    argument.completer = _package_completer  # type: ignore

    argcomplete.autocomplete(parser)

    args = parser.parse_args()
    args.action(args)
