from typing import Any, Callable, Type, Union, cast

from wapt.version.GenericVersion import GenericVersion, GenericVersionConfig
from wapt.version.Semver import Semver, SemverConfig

VersionsConfig = Union[GenericVersionConfig, SemverConfig]
Versions = Union[GenericVersion, Semver]

VersionBuilder = Callable[[str], Versions]


def get_version_builder(Version: Type[Versions], config: VersionsConfig) -> VersionBuilder:
    def version_builder(version: str):
        return Version(cast(Any, config), version)

    return version_builder
