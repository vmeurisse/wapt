import re
from dataclasses import dataclass
from typing import Literal, Union

from wapt.version.Version import Version, VersionConfig


@dataclass
class _GenericVersionConfig:
    type: Literal["generic"]


@dataclass
class GenericVersionConfig(VersionConfig, _GenericVersionConfig):
    pass


def _parse_part(part: str) -> Union[str, int]:
    if part[0].isdigit():
        return int(part)
    return part


class GenericVersion(Version):
    SPLIT_PATTERN = re.compile("(?:[^a-z0-9]+|(?<=[a-z])(?=[0-9])|(?<=[0-9])(?=[a-z]))")

    def __init__(self, config: GenericVersionConfig, version: str) -> None:
        super().__init__(config, version)
        parts = self.SPLIT_PATTERN.split(self._version)
        self._parts = [_parse_part(part) for part in parts if part]

    def compare(self, other: object) -> int:
        if not isinstance(other, GenericVersion):
            raise TypeError(f"Cannot commpare with {type(other)}")

        for ours, theirs in zip(self._parts, other._parts):  # pylint: disable=protected-access
            t_ours = type(ours)
            t_theirs = type(theirs)

            if t_ours == t_theirs:
                if ours == theirs:
                    continue
                return -1 if ours < theirs else 1  # type: ignore (inference not working for the equal types)
            else:
                return -1 if t_ours == str else 1

        l_ours = len(self._parts)
        l_theirs = len(other._parts)  # pylint: disable=protected-access
        return 0 if l_ours == l_theirs else -1 if l_ours < l_theirs else 1
