from dataclasses import dataclass
from typing import Literal

from semver import VersionInfo as SemverVersion

from wapt.version.Version import Version, VersionConfig


@dataclass
class _SemverConfig:
    type: Literal["semver"]


@dataclass
class SemverConfig(VersionConfig, _SemverConfig):
    pass


class Semver(Version):
    def __init__(self, config: SemverConfig, version: str) -> None:
        super().__init__(config, version)
        self._semver = SemverVersion.parse(self._version)

    def compare(self, other: object) -> int:
        if not isinstance(other, Semver):
            raise TypeError(f"Cannot commpare with {type(other)}")
        return self._semver.compare(other._semver)  # pylint: disable=protected-access
