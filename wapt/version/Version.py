from dataclasses import dataclass, field
from typing import List, TypeVar

from wapt.utils.Compare import Compare


@dataclass
class VersionConfig:
    prefix: List[str] = field(default_factory=list)
    suffix: List[str] = field(default_factory=list)


T = TypeVar("T")


class Version(Compare):
    def __init__(self, config: VersionConfig, version: str) -> None:
        self._original_version = version
        for prefix in config.prefix:
            if version.startswith(prefix):
                version = version[len(prefix) :]
                break
        for suffix in config.suffix:
            if version.endswith(suffix):
                version = version[: -len(suffix)]
                break
        self._version = version

    def __str__(self) -> str:
        return self._original_version
