import sys
from dataclasses import dataclass
from shutil import rmtree
from typing import Any, List

from wapt.config.config import Config, config_parse
from wapt.config.data import read_installed_version, store_installed_version
from wapt.downloaders.common.Release import Release
from wapt.utils.folders import application_install_folder, tempFolder


@dataclass
class _Candidate:
    config: Config
    release: Release[Any]


def _get_candidates(packages: List[str]):
    configs = [config_parse(package) for package in packages]

    candidates: List[_Candidate] = []
    for config in configs:
        releases = sorted(config.downloader.list_releases(), reverse=True)
        if not releases:
            print(f"No release available for package <{config.name}>")
            sys.exit(1)

        release = releases[0]
        curent = read_installed_version(config.name)
        if curent and release.version <= config.version_builder(curent):
            print(f"Package <{config.name}> is already installed ({curent}).")
        else:
            candidates.append(_Candidate(config, release))
    return candidates


def _do_install(candidate: _Candidate):
    config = candidate.config
    release = candidate.release

    with tempFolder() as tmp:
        downloaded = config.downloader.download(release, tmp)

        install_folder = application_install_folder(config.name)
        install_folder_new = install_folder.with_name(install_folder.name + ".new")
        install_folder_old = install_folder.with_name(install_folder.name + ".old")

        if install_folder_new.exists():
            rmtree(install_folder_new)
        if install_folder_old.exists():
            rmtree(install_folder_old)
        install_folder_new.mkdir(parents=True)

        try:
            config.installer.pre_install(config.name, downloaded, install_folder_new)

            if install_folder.exists():
                install_folder.rename(install_folder_old)

            success = False
            try:
                install_folder_new.rename(install_folder)

                config.installer.post_install(config.name, install_folder)
                store_installed_version(config.name, str(release.version))
                success = True
            finally:
                if not success and install_folder_old.exists():
                    if install_folder.exists():
                        rmtree(install_folder)
                    install_folder_old.rename(install_folder)
        finally:
            if install_folder_new.exists():
                rmtree(install_folder_new)
            if install_folder_old.exists():
                rmtree(install_folder_old)


def install(packages: List[str]):
    candidates = _get_candidates(packages)

    for candidate in candidates:
        _do_install(candidate)
