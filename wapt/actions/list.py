from typing import List

from wapt.config.config import config_parse
from wapt.config.data import read_installed_version
from wapt.utils.packages import validate_package_names


def list_action(packages: List[str], all_versions: bool):
    validate_package_names(*packages)

    configs = [config_parse(package) for package in packages]

    for config in configs:
        releases = sorted(config.downloader.list_releases(), reverse=True)
        if releases:

            curent_str = read_installed_version(config.name)
            curent = config.version_builder(curent_str) if curent_str else None
            if not all_versions:
                releases = [releases[0]]

            for release in releases:
                installed = " [installed]" if curent and release.version == curent else ""
                print(f"{config.name} {release.version}{installed}")
