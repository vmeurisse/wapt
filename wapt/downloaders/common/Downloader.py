from abc import ABC, abstractmethod
from pathlib import Path
from typing import Generic, List, TypeVar

from wapt.downloaders.common.Release import Release

T = TypeVar("T")


class Downloader(ABC, Generic[T]):
    def __init__(self) -> None:
        ...

    @abstractmethod
    def list_releases(self) -> List[Release[T]]:
        ...

    @abstractmethod
    def download(self, release: Release[T], folder: Path) -> Path:
        ...
