import re
from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from typing import Any, List, Literal, Pattern, TypeVar, Union


def get_data(data: Any, key_parts: List[str]) -> Any:
    if data is None:
        return None
    if not key_parts:
        return data

    key = key_parts[0]
    try:
        value = getattr(data, key)
    except AttributeError:
        try:
            value = data[key]
        except (TypeError, IndexError, KeyError):
            value = None
    return get_data(value, key_parts[1:])


@dataclass
class Operator(ABC):
    @abstractmethod
    def filter_object(self, value: Any) -> bool:
        ...


@dataclass
class Filter(Operator):
    key: str
    _key_parts: List[str] = field(init=False)

    def __post_init__(self):
        self._key_parts = self.key.split(".")

    def filter_object(self, value: Any) -> bool:
        value = get_data(value, self._key_parts)

        return self.filter(value)

    @abstractmethod
    def filter(self, value: Any) -> bool:
        ...


@dataclass
class BooleanFilter(Filter):
    value: bool

    def filter(self, value: Any) -> bool:
        return self.value == value


@dataclass
class StringFilter(Filter):
    value: str

    def filter(self, value: Any) -> bool:
        return self.value == value


@dataclass
class PatternFilter(Filter):
    type: Literal["regex"]
    value: str
    pattern: Pattern[str] = field(init=False)

    def __post_init__(self):
        super().__post_init__()
        self.pattern = re.compile(self.value)

    def filter(self, value: Any) -> bool:
        return isinstance(value, str) and self.pattern.fullmatch(value) is not None


@dataclass
class AndFilter(Operator):
    type: Literal["and"]
    filters: "Filters"

    def filter_object(self, value: Any) -> bool:
        for f in self.filters:
            if not f.filter_object(value):
                return False
        return True


@dataclass
class OrFilter(Operator):
    type: Literal["or"]
    filters: "Filters"

    def filter_object(self, value: Any) -> bool:
        for f in self.filters:
            if f.filter_object(value):
                return True
        return False


@dataclass
class NotFilter(Operator):
    type: Literal["not"]
    filters: "Filters"

    def __post_init__(self):
        self._and_filter = AndFilter("and", self.filters)

    def filter_object(self, value: Any) -> bool:
        return not self._and_filter.filter_object(value)


FilterTypes = Union[AndFilter, OrFilter, NotFilter, BooleanFilter, StringFilter, PatternFilter]

# Hack for typedload. It doesn't support types as string. Replace annotation with real type
Filters = List[FilterTypes]
for operator in [AndFilter, OrFilter, NotFilter]:
    operator.__dataclass_fields__.get("filters").type = Filters  # type: ignore

T = TypeVar("T")


def filter_list(filters: Filters, data: List[T]) -> List[T]:
    f = AndFilter("and", filters)
    return [item for item in data if f.filter_object(item)]
