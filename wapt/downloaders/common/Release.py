from functools import total_ordering
from typing import Generic, TypeVar

from wapt.utils.Compare import Compare
from wapt.version import Versions

T = TypeVar("T")


@total_ordering
class Release(Generic[T], Compare):
    def __init__(self, version: Versions, data: T) -> None:
        self._version = version
        self._data = data

    @property
    def version(self):
        return self._version

    @property
    def data(self):
        return self._data

    def __repr__(self) -> str:
        return f"Release(version={self._version}, data={self._data})"

    def compare(self, other: object) -> int:
        if not isinstance(other, Release):
            raise TypeError(f"Cannot commpare with {type(other)}")
        return self._version.compare(other._version)  # pylint: disable=protected-access
