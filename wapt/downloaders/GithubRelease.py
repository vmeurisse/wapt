import json
from dataclasses import dataclass, field
from pathlib import Path
from typing import Any, Dict, List, Literal

import requests
import typedload

from wapt.downloaders.common.Downloader import Downloader, Release
from wapt.downloaders.common.Filter import Filters, filter_list
from wapt.utils.frw import REQUESTS_PARAMS
from wapt.version import VersionBuilder


@dataclass
class GithubReleaseConfig:
    type: Literal["github-release"]
    org: str
    repo: str
    version: Literal["tag", "name"] = "tag"
    filters: Filters = field(default_factory=list)
    asset_filter: Filters = field(default_factory=list)


@dataclass
class _GhAsset:
    name: str
    content_type: str
    browser_download_url: str


@dataclass
class _GhRelease:
    tag_name: str
    name: str
    draft: bool
    prerelease: bool
    assets: List[_GhAsset]


class GithubRelease(Downloader[_GhRelease]):
    def __init__(self, config: GithubReleaseConfig, version_builder: VersionBuilder) -> None:
        super().__init__()
        self._config = config
        self._version_builder = version_builder

    def list_releases(self) -> List[Release[_GhRelease]]:
        releases = filter_list(self._config.filters, self._github_get_releases())
        return [self._as_release(release) for release in releases]

    def _as_release(self, api_response: Dict[str, Any]) -> Release[_GhRelease]:
        version_type = self._config.version
        if version_type == "tag":
            version: str = api_response["tag_name"]
        elif version_type == "name":
            version: str = api_response["name"]
        else:
            raise Exception("Invalid version_type")

        try:
            return Release(self._version_builder(version), typedload.load(api_response, _GhRelease))
        except Exception as e:
            raise Exception(json.dumps(api_response, indent=4)) from e

    def _print_release(self, data: Dict[str, Any]) -> None:
        copy = {**data}
        for key, value in copy.items():
            if isinstance(value, str) and len(value) > 100:
                copy[key] = value[0:100]
        print(json.dumps(copy, indent=4))

    def _github_get_releases(self) -> List[Dict[str, Any]]:
        url = f"https://api.github.com/repos/{self._config.org}/{self._config.repo}/releases?per_page=100"
        headers = {"Accept": "application/vnd.github.v3.full+json"}

        response = requests.get(url, headers=headers, **REQUESTS_PARAMS)
        data = response.json()
        # self._print_release(data[0])
        return data

    def download(self, release: Release[_GhRelease], folder: Path):
        asset = filter_list(self._config.asset_filter, release.data.assets)[0]

        file_path = folder / "file"
        with requests.get(asset.browser_download_url, stream=True, **REQUESTS_PARAMS) as dl:
            with open(file_path, "wb") as file:
                for chunk in dl.iter_content(chunk_size=16 * 1024):
                    file.write(chunk)
        return file_path
