from abc import ABC, abstractmethod
from pathlib import Path


class Installer(ABC):
    @abstractmethod
    def pre_install(self, name: str, download: Path, dest: Path) -> None:
        ...

    @abstractmethod
    def post_install(self, name: str, dest: Path) -> None:
        ...
