from configparser import ConfigParser
from dataclasses import dataclass
from pathlib import Path
from subprocess import run
from typing import List, Literal, Optional

from wapt.installer.common.installer import Installer
from wapt.utils.folders import desktop_file_path, tempFolder


@dataclass
class AppImageConfig:
    type: Literal["AppImage"]


class AppImage(Installer):
    def __init__(self, _: AppImageConfig) -> None:
        self._dest = None
        self._name = None
        self._icon = None
        self._desktop = None

    def _find_desktop_file(self, folder: Path):
        desktop_files = list(folder.glob("*.desktop"))

        if not desktop_files:
            return None
        if len(desktop_files) == 1:
            return desktop_files[0]

        print("AppImage contain multiple desktop files")
        return None

    def _resolve(self, path: Path, folder: Path):
        if not path.exists():
            return None

        resolved = path.resolve()
        if not resolved.is_file():
            return None

        try:
            # Check that path is inside the mount point
            # Prevent path traversal attacks
            resolved.relative_to(folder)
            return resolved
        except ValueError:
            return None

    def _find_icon(self, appname: Optional[str], icon: Optional[str], folder: Path):
        exts = [".png", ".xpm", ".svg"]

        theme_folder = folder / "usr/share/icons/hicolor"
        sizes: List[int] = []
        if theme_folder.exists():
            sub_dirs = [x for x in theme_folder.iterdir() if x.is_dir()]
            for sub_dir in sub_dirs:
                parts = sub_dir.name.split("x")
                if len(parts) == 2 and parts[0] == parts[1]:
                    size = parts[0]
                    if size.isascii() and size.isdecimal():
                        sizes.append(int(size))
        sizes.sort(reverse=True)
        sizes_path = [theme_folder / f"{size}x{size}" for size in sizes]

        for ext in exts:
            if icon:
                for path in sizes_path:
                    icon_path = self._resolve(path / f"{icon}{ext}", folder)
                    if icon_path:
                        return icon_path
            if ext == ".png":
                icon_path = self._resolve(folder / f"{appname}{ext}", folder)
                if icon_path:
                    return icon_path
        icon_path = self._resolve(folder / ".DirIcon", folder)
        if icon_path:
            return icon_path

        return None

    def pre_install(self, name: str, download: Path, dest: Path) -> None:
        self._dest = f"{name}.AppImage"
        self._name = name

        appimage = dest / self._dest
        download.rename(appimage)
        appimage.chmod(0o755)

        # https://github.com/AppImage/AppImageSpec/blob/master/draft.md
        with tempFolder() as tmp:
            run([appimage, "--appimage-extract"], cwd=tmp, check=True)
            extracted = tmp / "squashfs-root"

            desktop_file = self._find_desktop_file(extracted)
            appname = desktop_file.stem if desktop_file else None

            if desktop_file:
                self._desktop = ConfigParser(interpolation=None)
                self._desktop.optionxform = lambda optionstr: optionstr
                with open(desktop_file) as f:
                    self._desktop.read_file(f)
                icon = self._desktop.get("Desktop Entry", "Icon", fallback=None)

                icon_path = self._find_icon(appname, icon, extracted)
                if icon_path:
                    self._icon = f"{appname}{icon_path.suffix}"
                    icon_path.rename(dest / self._icon)

    def post_install(self, name: str, dest: Path) -> None:
        if self._desktop:
            if self._icon:
                self._desktop.set("Desktop Entry", "Icon", (dest / self._icon).as_posix())

            executable = self._desktop.get("Desktop Entry", "Exec")
            parts = executable.split(" ", maxsplit=1)
            if self._dest is None:
                raise AssertionError("Value should have been set in pre_install")
            parts[0] = (dest / self._dest).as_posix()
            self._desktop.set("Desktop Entry", "Exec", " ".join(parts))

            with open(desktop_file_path(name), "w") as f:
                self._desktop.write(f)
