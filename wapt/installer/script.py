from dataclasses import dataclass, field
from pathlib import Path
from subprocess import run
from typing import List, Literal

from wapt.installer.common.installer import Installer


@dataclass
class ScriptInstallerConfig:
    type: Literal["script"]
    params: List[str] = field(default_factory=list)


class ScriptInstaller(Installer):
    def __init__(self, config: ScriptInstallerConfig) -> None:
        self._config = config
        self._script = None

    def pre_install(self, name: str, download: Path, dest: Path) -> None:
        self._script = download
        self._script.chmod(0o755)

    def post_install(self, name: str, dest: Path) -> None:
        if self._script is None:
            raise AssertionError("Value should have been set in pre_install")
        run([self._script, *self._config.params], check=True, cwd=dest)
