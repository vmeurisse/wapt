from typing import Union

from wapt.installer.appimage import AppImage, AppImageConfig
from wapt.installer.script import ScriptInstaller, ScriptInstallerConfig

InstallersConfig = Union[AppImageConfig, ScriptInstallerConfig]
Installers = Union[AppImage, ScriptInstaller]
