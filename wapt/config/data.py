import json
from dataclasses import dataclass

from typedload import dump, load

from wapt.utils.folders import installed_data_file


@dataclass
class Version:
    version: str


def store_installed_version(package: str, version: str):
    with installed_data_file(package).open("w") as f:
        json.dump(dump(Version(version)), f)


def read_installed_version(package: str):
    f = installed_data_file(package)
    if not f.exists():
        return None

    with installed_data_file(package).open() as f:
        data = json.load(f)
        return load(data, Version).version
