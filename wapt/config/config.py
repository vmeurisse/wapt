import sys
from dataclasses import dataclass, field
from typing import Any, Iterable, List, Type, TypeVar, Union, get_args, get_origin, get_type_hints

from typedload import load
from yaml import safe_load

from wapt.downloaders import Downloaders, DownloadersConfig
from wapt.installer import Installers, InstallersConfig
from wapt.utils.folders import config_packages
from wapt.version import VersionBuilder, Versions, VersionsConfig, get_version_builder
from wapt.version.GenericVersion import GenericVersionConfig

T = TypeVar("T")


def _uniontypes(type_: Type[T]) -> Iterable[Type[T]]:
    if get_origin(type_) == Union:
        return get_args(type_)
    else:
        return (type_,)


@dataclass
class ConfigFile:
    downloader: DownloadersConfig
    installer: InstallersConfig
    version: VersionsConfig = field(default_factory=lambda: GenericVersionConfig("generic"))


@dataclass
class Config:
    name: str
    downloader: Downloaders
    installer: Installers
    version_builder: VersionBuilder


def config_list_packages() -> List[str]:
    folder = config_packages()
    if not folder.exists():
        return []

    return [path.stem for path in folder.glob("*.yml")]


def _find_class_union(config: Any, union_types: Type[T]) -> Type[T]:
    types = _uniontypes(union_types)
    for Class in types:
        types = get_type_hints(Class.__init__)
        del types["return"]
        types = list(types.values())
        if types[0] == type(config):
            return Class
    raise Exception(f"Cannot find class for config {config}")


def _instantiate_union(config: Any, union_types: Type[T], *args: Any) -> T:
    Class = _find_class_union(config, union_types)
    return Class(config, *args)


def _instantiate_downloader(config: DownloadersConfig, version_builder: VersionBuilder) -> Downloaders:
    return _instantiate_union(config, Downloaders, version_builder)


def _instantiate_installer(config: InstallersConfig) -> Installers:
    return _instantiate_union(config, Installers)


def _find_version(config: VersionsConfig) -> Type[Versions]:
    return _find_class_union(config, Versions)


def config_parse(package: str):
    folder = config_packages()
    path = (folder / package).with_suffix(".yml")

    if not path.exists():
        raise Exception(f"Package <{package}> doesn't exist")

    try:
        with open(path) as f:
            data = safe_load(f)
        config_file = load(data, ConfigFile, failonextra=True, basiccast=False)
    except Exception as e:  # pylint: disable=broad-except
        print(f"Error parsing: {path}: {e}")
        sys.exit(1)

    version_builder = get_version_builder(_find_version(config_file.version), config_file.version)
    return Config(
        name=package,
        downloader=_instantiate_downloader(config_file.downloader, version_builder),
        installer=_instantiate_installer(config_file.installer),
        version_builder=version_builder,
    )
