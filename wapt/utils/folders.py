from contextlib import contextmanager
from pathlib import Path
from tempfile import TemporaryDirectory

from xdg import xdg_cache_home, xdg_config_home, xdg_data_home

_CONFIG_BASE = xdg_config_home() / "wapt"
_CONFIG_PACKAGES = _CONFIG_BASE / "packages"


_DATA_BASE = xdg_data_home() / "wapt"
_DESKTOP_FILES_BASE = xdg_data_home() / "applications"

_CACHE_BASE = xdg_cache_home() / "wapt"
_TMP_BASE = _CACHE_BASE / "tmp"


def config_packages():
    return _CONFIG_PACKAGES


def application_install_folder(application: str) -> Path:
    return _DATA_BASE / "application" / application


def installed_data_file(application: str) -> Path:
    folder = _DATA_BASE / "installed"
    if not folder.exists():
        folder.mkdir(parents=True)
    return folder / f"{application}.json"


def desktop_file_path(application: str) -> Path:
    return _DESKTOP_FILES_BASE / f"wapt-{application}.desktop"


@contextmanager
def tempFolder():
    _TMP_BASE.mkdir(parents=True, exist_ok=True)
    with TemporaryDirectory(dir=_TMP_BASE) as tmp:
        yield Path(tmp)
