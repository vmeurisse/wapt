import re
import sys
from typing import List


def _parse_packages(package: str):
    parts = package.split("=")
    if len(parts) > 2:
        print(f"Cannot parse package name: <{package}>")
        sys.exit(2)
    validate_package_names(parts[0])
    return parts


def parse_packages(packages: List[str]) -> List[List[str]]:
    return [_parse_packages(package) for package in packages]


def validate_package_names(*names: str) -> None:
    # Rule from https://www.debian.org/doc/debian-policy/ch-controlfields.html
    pattern = re.compile("[a-z0-9][a-z0-9.+-]+")
    for name in names:
        if not pattern.fullmatch(name):
            print(f"Invalid package name: {name}")
            print(
                "Package names  must consist only of lower case letters (a-z), digits (0-9), "
                + "plus (+) and minus (-) signs and periods (.). "
                + "They must be at least two characters long and must start with an alphanumeric character."
            )
            sys.exit(2)
