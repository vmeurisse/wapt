from abc import ABC, abstractmethod


class Compare(ABC):
    @abstractmethod
    def compare(self, other: object) -> int:
        ...

    def __eq__(self, other: object):
        return self.compare(other) == 0

    def __ne__(self, other: object):
        return self.compare(other) != 0

    def __lt__(self, other: object):
        return self.compare(other) < 0

    def __le__(self, other: object):
        return self.compare(other) <= 0

    def __gt__(self, other: object):
        return self.compare(other) > 0

    def __ge__(self, other: object):
        return self.compare(other) >= 0
