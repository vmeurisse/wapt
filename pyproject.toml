[tool.poetry]
name = "wapt"
version = "0.0.1"
description = "Web apt"
license = "MIT"
authors = ["Vincent Meurisse <dev@meurisse.org>"]

[tool.poetry.dependencies]
python = ">=3.9.0, <3.11"
typedload = "~=2.8"
xdg = "~=5.1"
pyyaml = "~=6.0"
semver = "~=2.13"
requests = "~=2.26"
argcomplete = "~=3.0.8"

[tool.poetry.dev-dependencies]
autoflake = "2.0.0"
bandit = {extras = ["toml"], version = "^1.7.4"}
black = "22.12.0"
isort = "5.11.4"
pylint = "2.15.9"


[tool.poetry.scripts]
wapt = 'wapt.main:main'

[build-system]
requires = ["poetry-core>=1.0.0"]
build-backend = "poetry.core.masonry.api"

[tool.bandit]
skips = [
  # Subprocess module
  "B404",
  # subprocess_without_shell_equals_true
  "B603",
  # start_process_with_no_shell
  "B606",
  # start_process_with_partial_path
  "B607",
]

[tool.black]
line-length = 120
target-version = ['py38']

[tool.isort]
profile = 'black'
line_length = 120
py_version = 'auto'

[tool.pylint.config]
score = 'n'
output-format = 'colorized'
enable = ['useless-suppression']

# https://pylint.pycqa.org/en/latest/technical_reference/features.html
disable = [
    'fixme',

    # Else is important for readability
    'no-else-break',
    'no-else-continue',
    'no-else-raise',
    'no-else-return',

    # Format is ensured by black
    'format',

    # Not all files are modules. This causes a lot of false positive
    'invalid-name',
    'missing-module-docstring',
    'missing-class-docstring',
    'missing-function-docstring',

    # Imports are checked by pyright
    'unused-import',
    'import-error',
    'relative-beyond-top-level',

    # Types are checked by pyright
    'no-member',

    # Order is checked by isort
    'wrong-import-order',

    # Causes issues with subclasses that don't modify public methods
    'too-few-public-methods',

    # Complaining on dataclasses
    'too-many-instance-attributes',

    # Needed for caches
    'global-statement',

    'unspecified-encoding',
]
